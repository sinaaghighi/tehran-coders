# Tehran Coders

Tehran Coders is an international Web and Software Development company based in Tehran, Iran. This company tries to finish projects using the highest level of techologies and best quality.

# Requirements to contribute this project

To contribute this project, You need to know about some technologies. Here we listed these technologies:

- HTML5 & CSS3
- Vue, JavaScript framework
- npm (Node Package Manager)
- SASS Language
- Webpack

# Webpack Configuration in "./webpack.config.js"
![Webpack Configuration](/assets/images/readme-image-webpack.png)

# Project Directory Structure

- The output directory for CSS files is /dist/stylesheet
- The output directory for JS files is /dist/javascript
- CSS and JavaScript Libraries are imported from /lib directory and seprated by format.
- You've to use SASS to watch & export the changes from '/dev/stylesheet' to output directory.
- You've to use Webpack to watch and export the changes from 'dev/javascript/ to output directory.
- The directory '/node_modules' is ignored, So you've to install whatever you need to contribute.

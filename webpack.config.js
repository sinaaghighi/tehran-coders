const path = require('path');

module.exports = {
    entry: {
        index: "/dev/javascript/index.js",
        global: "/dev/javascript/global.js"
    },
    output: {
        filename: "[name].js",
        path: __dirname + "/dist/javascript"
    }
}
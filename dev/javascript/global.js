import '../../lib/javascript/vue.global'

// navList
const navList = {
    data() {
        return {
            home: 'خـانه',
            about: 'دربارهٔ ما',
            services: 'خدمات',
            techs: 'فناوری‌ها',
            partners: 'همکاران',
            team: 'تیم ما',
            callNow: 'تماس با ما'
        }
    }
}
Vue.createApp(navList).mount('nav')
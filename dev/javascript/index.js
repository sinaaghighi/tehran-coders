import '../../lib/javascript/vue.global'

// Header Content
const headerContent = {
    data() {
        return {
            title: 'با ما، همه چیز از جهان نرم‌افزار...',
            subTitle: 'نخستین شرکت حامی آزادی نرم‌افزار و نرم‌افزارهای آزاد در ایران'
        }
    }
}
Vue.createApp(headerContent).mount('header')

// About Us and Introduction
const aboutUs = {
    data() {
        return {
            title: 'دربارهٔ ما',
            paragraph1: 'تهران‌کدرز، یک گروه متشکل از مجرب‌ترین طراحان و برنامه‌نویسان است که کار خود را از ۱۳۹۹ خورشیدی در ایران آغاز کرد. هدف از تاسیس این تیم، ارائه باکیفیت‌ترین خدمات در زمینهٔ فناوری اطلاعات و مهندسی نرم‌افزار است.',
            paragraph2: 'تهران‌کدرز به عنوان اولین تیم ایرانی که حمایت خود را از نرم‌افزارهای آزاد و آزادی نرم‌افزار اعلام کرد، همواره در تلاش بوده پروژه‌های خود را با پروانه‌های آزاد/متن‌باز منتشر کند.'
        }
    }
}
Vue.createApp(aboutUs).mount('#about')

// Services Section
const servicesContent = {
    data() {
        return {
            title: 'خدمات تهران‌کدرز',
            subTitle: 'کارهایی که در تهران‌کدرز برایتان انجام می‌دهیم',
            // Services Items Titles
            item_webDev: 'توسعهٔ وب',
            item_wp: 'خدمات وردپرس',
            item_ui: 'طراحی رابط کاربری',
            item_sysAdmin: 'مدیریت سیستم',
            item_consult: 'مشاورهٔ فنی',
            item_localization: 'بومی‌سازی نرم‌افزار',
            item_devops: 'مهندسی دواپس',
            item_android: 'توسعهٔ اندروید',
            item_seo: 'سئو و بهینه‌سازی',
            item_cyber: 'امنیت سایبری',
            item_licensing: 'پروانه‌گذاری حقوقی',
            // Services Items Sub-titles
            item_webDev_sub: 'Web Development',
            item_wp_sub: 'WordPress Services',
            item_ui_sub: 'UI/UX Design',
            item_sysAdmin_sub: 'System Administration',
            item_consult_sub: 'Technical Consulting',
            item_localization_sub: 'Software Localization',
            item_devops_sub: 'DevOps Engineering',
            item_android_sub: 'Android Development',
            item_seo_sub: 'SEO & Web Optimization',
            item_cyber_sub: 'Cyber Security',
            item_licensing_sub: 'Legal Licensing'
        }
    }
}
Vue.createApp(servicesContent).mount('#services')

// Partners Section
const partnersContent = {
    data() {
        return {
            title: 'همکاران',
            subTitle: 'از خدمات یکدیگر استفاده می‌کنیم'
        }
    }
}
Vue.createApp(partnersContent).mount('#partners')

// Technologies Section
const techContent = {
    data() {
        return {
            title: 'فناوری‌ها',
            subTitle: 'از چه فناوری‌هایی استفاده می‌کنیم؟'
        }
    }
}
Vue.createApp(techContent).mount('#techs')

// Team Section
const teamMembers = {
    data() {
        return {
            title: 'تیم تهران‌کدرز',
            subTitle: 'آشنایی مختصر با اعضای تیم'
        }
    }
}
Vue.createApp(teamMembers).mount('#members')

// Footer
const footer = {
    data () {
        return {
            // Section: Tehran Coders
            footerTHCoders: 'تهران‌کدرز',
            footerTHCoders_careers: 'فرصت‌های شغلی',
            footerTHCoders_careers_active: 'در حال جذب',
            footerTHCoders_careers_inactive: 'غیرفعال',
            footerTHCoders_about: 'دربارهٔ ما',
            footerTHCoders_services: 'خدمات و امکانات',
            footerTHCoders_partners: 'مشتریان و همکاران',
            // Section: Contact Information
            footerContact: 'تماس با ما',
            footerContactEmail: 'info@tehrancoders.com',
            footerContactPhone: '۰۹۳۹۱۴۶۶۲۵۳',
            // Section: Social Networks
            footerSocial: 'شبکه‌های اجتماعی',
            footerSocial_linkedin: 'لینکدین',
            footerSocial_gitlab: 'گیت‌لب',
            footerSocial_instagram: 'اینستاگرام',
            footerSocial_mastodon: 'ماستودون',
            footerSocial_twitter: 'توییتر',
            // Section: GNU GPL
            footerLicense: 'پروانهٔ انتشار',
            footerLicenseDescription: 'این وب‌سایت با پروانهٔ انتشار عمومی گنو (نسخهٔ ۳) منتشر شده است و برای تکثیر محتوای آن بایستی مفاد پروانه رعایت شوند.'
        }
    }
}
Vue.createApp(footer).mount('footer')